# foodsharing android app

This is a cordova app for [foodsharing.de](https://foodsharing.de).

## getting started

First make sure you have cordova installed, install it globally:

```
npm install -g cordova
```

(you might need to use `sudo` for that depending on how your npm is configured)


Now add the android platform:

```
cordova platform add android
```

Then you should be able to build and run it:

```
cordova run android
```

For that to work you need a phone connected with USB debugging enabled, or have the android SDK and an emulator available.

## building

For now we only do development builds, and include a developer keystore. You can build an installable .apk with:

```
cordova build --release -- --keystore=foodsharing.dev.pfx --alias=foodsharing.dev --storePassword=foodsharing.dev --password=foodsharing.dev
```

## tests

There are not many tests, there are a few browser-based jasmine tests.

You can run them by running a webserver in the root directory and visiting the `spec.html` page, e.g.:

```
python -m http.server
```

Then open [localhost:8000/spec.html](http://localhost:8000/spec.html) in a browser.